<?php

namespace App\DataFixtures;

use App\Entity\DataGetterSource;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 25527; $i <= 25544; $i++) {
            $dataGetterSource = new DataGetterSource();
            $dataGetterSource->setUrl('https://www.bip.krakow.pl/?bip_id=1&mmi=' . $i);
            $dataGetterSource->setTitleSelector('.bip');
            $dataGetterSource->setPopulationSelector('#newsblock > p:nth-child(7) > strong');
            $dataGetterSource->setCitySelector('#newsblock > p:nth-child(11) > strong');
            $dataGetterSource->setAreaSelector('#newsblock > p:nth-child(6) > strong');
            $manager->persist($dataGetterSource);
        }

        $manager->flush();
    }
}
