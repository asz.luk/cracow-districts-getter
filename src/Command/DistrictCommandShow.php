<?php

namespace App\Command;

use App\Repository\CracowDistrictRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:districts-command:show',
    description: 'Show Cracow Districts.',
    hidden: false,
)]
class DistrictCommandShow extends Command
{
    private CracowDistrictRepository $repository;

    public function __construct(CracowDistrictRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $districts = $this->repository->findAll();
        $data = json_decode(json_encode($districts), true);

        foreach($data as $value) {
            $output->writeln($value['name']);
            $output->writeln($value['city']);
            $output->writeln('Population: ' . $value['population']);
            $output->writeln('Area: ' . $value['area']);
            $output->writeln('===================');
        }
        
        return Command::SUCCESS;
    }
}
