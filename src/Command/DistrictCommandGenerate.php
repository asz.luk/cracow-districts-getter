<?php

namespace App\Command;

use App\Controller\MainController;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:districts-command:generate',
    description: 'Generates Cracow Districts.',
    hidden: false,
)]
class DistrictCommandGenerate extends Command
{
    private MainController $controller;

    public function __construct(MainController $controller)
    {
        parent::__construct();
        $this->controller = $controller;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->controller->generate();

        $output->writeln('Districts has been generated!');
        
        return Command::SUCCESS;
    }
}