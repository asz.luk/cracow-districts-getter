<?php

namespace App\Entity;

use App\Repository\DataGetterSourceRepository;
use App\DataGetter\DataGetterInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataGetterSourceRepository::class)
 */
class DataGetterSource implements DataGetterInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $url;

    /**
     * @ORM\Column(type="string")
     */
    private string $titleSelector;

    /**
     * @ORM\Column(type="string")
     */
    private string $populationSelector;

    /**
     * @ORM\Column(type="string")
     */
    private string $citySelector;

    /**
     * @ORM\Column(type="string")
     */
    private string $areaSelector;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getTitleSelector(): string
    {
        return $this->titleSelector;
    }

    public function setTitleSelector(string $titleSelector): void
    {
        $this->titleSelector = $titleSelector;
    }

    public function getPopulationSelector(): string
    {
        return $this->populationSelector;
    }

    public function setPopulationSelector(string $populationSelector): void
    {
        $this->populationSelector = $populationSelector;
    }

    public function getCitySelector(): string
    {
        return $this->citySelector;
    }

    public function setCitySelector(string $citySelector): void
    {
        $this->citySelector = $citySelector;
    }

    public function getAreaSelector(): string
    {
        return $this->areaSelector;
    }

    public function setAreaSelector(string $areaSelector): void
    {
        $this->areaSelector = $areaSelector;
    }
}
