<?php

namespace App\DataGetter;

interface DataGetterInterface
{
    public function getUrl(): string;
    public function getTitleSelector(): string;
    public function getPopulationSelector(): string;
    public function getCitySelector(): string;
    public function getAreaSelector(): string;
}