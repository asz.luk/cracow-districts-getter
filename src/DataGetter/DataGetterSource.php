<?php

namespace App\DataGetter;

use App\DataGetter\DataGetterInterface;

class DataGetterSource implements DataGetterInterface
{
    public function getUrl(): string
    {
        return '';
    }

    public function getTitleSelector(): string
    {
        return '';
    }

    public function getPopulationSelector(): string
    {
        return '';
    }

    public function getCitySelector(): string
    {
        return '';
    }

    public function getAreaSelector(): string
    {
        return '';
    }
}