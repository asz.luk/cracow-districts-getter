<?php

namespace App\DataGetter;

use App\Entity\CracowDistrict;
use Symfony\Component\Panther\Client;

class DataGetter
{
    public function getData(DataGetterInterface $dataGetter): object
    {
        $client = Client::createChromeClient(__DIR__.'/../../chromedriver');
        $crawler = $client->request('GET', $dataGetter->getUrl());

        $title = $crawler->filter($dataGetter->getTitleSelector())->text();
        $population = $crawler->filter($dataGetter->getPopulationSelector())->text();
        $city = $crawler->filter($dataGetter->getCitySelector())->text();
        $area = $crawler->filter($dataGetter->getAreaSelector())->text();
        $district = new CracowDistrict();
        $district->setName($title);
        $district->setPopulation($population);
        $district->setCity($city);
        $district->setArea($area);
        $client->quit();

        return $district;
    }
}
