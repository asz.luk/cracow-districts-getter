<?php

namespace App\Controller;

use App\Entity\DataGetterSource;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(CracowDistrictCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Cracow Districts');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Districts', 'fa fa-home');
        yield MenuItem::linkToCrud('Sources', 'fas fa-list', DataGetterSource::class);
        yield MenuItem::linkToRoute('Generate Districts', 'fas fa-wand-magic-sparkles', 'district_generator');
    }
}
