<?php

namespace App\Controller;

use App\Entity\DataGetterSource;
use App\DataGetter\DataGetter;
use App\Repository\DataGetterSourceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MainController extends AbstractController
{
    public function __construct(
        private DataGetter $dataGetter,
        private ManagerRegistry $doctrine,
        private DataGetterSourceRepository $dataSourceRepository
    ) {}

    #[Route('/get/{id}', name: 'get')]
    public function get(DataGetterSource $dataGetterSource)
    {
        $entityManager = $this->doctrine->getManager();

        $data = $this->dataGetter->getData($dataGetterSource);

        $entityManager->persist($data);
        $entityManager->flush();

        return $this->json($data);
    }

    #[Route('/generateDistricts', name: 'district_generator')]
    public function generate(): RedirectResponse
    {
        $sources = $this->dataSourceRepository->findAll();

        foreach($sources as &$value) {
            $this->get($value, $this->doctrine);
        }

        return $this->redirectToRoute('admin');
    }
}
