<?php

namespace App\Controller;

use App\Entity\CracowDistrict;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CracowDistrictCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CracowDistrict::class;
    }
}
