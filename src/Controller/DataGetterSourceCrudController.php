<?php

namespace App\Controller;

use App\Entity\DataGetterSource;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DataGetterSourceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DataGetterSource::class;
    }
}
