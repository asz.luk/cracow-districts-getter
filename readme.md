# Cracow Districts Generator
> Tool that generates a list of Cracow districts.

### Installation
* Get Docker from [Official Docker Website](https://docs.docker.com/get-docker/) for your system and install it.
* Fill .env file.
* Build and run Docker containers.
```
docker-compose up --build -d
```
* Build the project.
```
docker exec dg_php sh -c "composer install"
```
* Prepare the database.
```
docker exec dg_php sh -c "bin/console doctrine:migrations:migrate"
```
* Load sources.
```
docker exec dg_php sh -c "bin/console doctrine:fixtures:load --append"
```
* Install the browser driver.
```
docker exec dg_php sh -c "vendor/dbrekelmans/bdi/bdi detect"
```

### Usage
* http://localhost:57400/admin > Generate Districts

### Screenshots
![alt text](public/img/screen.png "Screenshots")

